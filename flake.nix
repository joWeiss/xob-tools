{
  description = "A very basic flake";
  inputs.nixpkgs.url = "nixpkgs";

  outputs = {
    self,
    nixpkgs,
  }: let
    system = "x86_64-linux";
    pkgs = nixpkgs.legacyPackages.${system};
  in {
    apps.${system}.xob-brightness = let
      xob-brightness = pkgs.writeShellScript "xob-brightness" ''
        ${self.packages.${system}.read-brightness}/bin/read-brightness \
        | ${pkgs.xob}/bin/xob >/dev/null
      '';
    in {
      type = "app";
      program = toString xob-brightness;
    };
    packages.${system}.read-brightness = pkgs.writeShellApplication {
      name = "read-brightness";
      runtimeInputs = [pkgs.bc pkgs.inotify-tools];
      text = with builtins; replaceStrings ["#!/usr/bin/env bash" "set -e"] ["" ""] (readFile ./read-brightness.sh);
    };
    devShells.${system}.default = pkgs.mkShell {
      packages = with pkgs; [bc xob inotify-tools shellcheck];
      shellHook = '''';
    };
  };
}
