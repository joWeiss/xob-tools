#!/usr/bin/env bash
set -e

FOLDER="/sys/class/backlight/intel_backlight"
MAX=$(<"$FOLDER"/max_brightness)
F="${FOLDER}/brightness"
CUR=$(<"$F")
PREV=$(<"$F")

inotifywait -m -e modify --format "%e" "$F" | while read -r EVENT; do
  case $EVENT in
  MODIFY)
    PREV="$CUR"
    CUR=$(<"$F")
    RATIO=$(bc <<<"scale=2; $CUR/$MAX*100")
    [ "$CUR" == "$PREV" ] || echo "${RATIO%%.*}"
    ;;
  *) ;;
  esac
done
